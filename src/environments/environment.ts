export const environment = {
    production: false,
    hmr       : false,
    testing: false,
    api: 'http://localhost:4000/api',
    settingsId: '5e91d7e5d46b19f8e3bec77a'
};