export const environment = {
    production: false,
    hmr       : false,
    testing: false,
    api: 'https://cp-api.ahmedzunaid.com/api',
    settingsId: '5e95430f57497012194dde5a'
};