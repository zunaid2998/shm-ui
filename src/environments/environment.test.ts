export const environment = {
    production: false,
    hmr       : false,
    testing: true,
    api: 'https://mvp.sharedhealthtest.ca/appapi',
    settingsId: '5e921e396ea1273d9a32a797'
};