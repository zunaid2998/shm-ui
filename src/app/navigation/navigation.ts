import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id       : 'applications',
        title    : 'Menus',
        type     : 'group',
        children : [
            {
                id       : 'dashboard',
                title    : 'Dashboard',
                type     : 'item',
                url      : 'dashboard',
                hidden   : true
            },
            {
                id       : 'materials',
                title    : 'Materials',
                type     : 'item',
                url      : 'materials',
                hidden   : true
            },
            {
                id       : 'users',
                title    : 'Users',
                type     : 'item',
                url      : 'users',
                hidden   : true
            },
            {
                id       : 'orders',
                title    : 'New Order',
                type     : 'item',
                url      : 'orders',
                hidden   : true
            },
            {
                id       : 'events',
                title    : 'Events',
                type     : 'item',
                url      : 'events',
                hidden   : true
            },
            {
                id       : 'orders-admin',
                title    : 'Orders',
                type     : 'item',
                url      : 'orders-admin',
                hidden   : true
            },
            {
                id       : 'orders-submitted',
                title    : 'Previous Orders',
                type     : 'item',
                url      : 'orders-submitted',
                hidden   : true
            },
            {
                id       : 'settings',
                title    : 'My Address List',
                type     : 'item',
                url      : 'settings',
                hidden   : true
            },
            {
                id       : 'station',
                title    : 'Station',
                type     : 'item',
                url      : 'station',
                hidden   : true
            },
            {
                id       : 'strategy',
                title    : 'Charging Strategy',
                type     : 'item',
                url      : 'strategy',
                hidden   : true
            },
            {
                id       : 'charge-point',
                title    : 'Charge Point',
                type     : 'item',
                url      : 'charge-point',
                hidden   : true
            },
            {
                id       : 'connector',
                title    : 'Connector',
                type     : 'item',
                url      : 'connector',
                hidden   : true
            },
            {
                id       : 'appointment',
                title    : 'Appointment',
                type     : 'item',
                url      : 'appointment',
                hidden   : true
            },
            {
                id       : 'payment-transaction',
                title    : 'Payments',
                type     : 'item',
                url      : 'payment',
                hidden   : true
            }
        ]
    }
];
