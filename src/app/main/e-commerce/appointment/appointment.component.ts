import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { AppointmentDetailComponent } from '../appointment/appointment-detail/appointment-detail.component';
import { Appointment } from '../model/appointment';
import { AppointmentService } from '../service/appointment.service';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.component.html',
  styleUrls: ['./appointment.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AppointmentComponent implements OnInit {
  appointmentList: any[] = [];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['appointmentNo', 'appointmentDateTime', 'estimatedCost', 'actualCost', 'status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private appointmentService: AppointmentService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.appointmentList);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadAppointmentList()
  }

  loadAppointmentList(): void {
    this.loading = true;
    this.appointmentService.getAppointment()
      .subscribe((response: any) => {
        this.loading = false;
        this.appointmentList = response.data;
        this.dataSource.data = this.appointmentList;
      }, error => console.log(error));
  }

  onClickDetail(appointment: any): void {
    let selectedAppointment: Appointment = new Appointment();
    selectedAppointment = _.clone(appointment);
    const appointmentDialog = this._matDialog.open(AppointmentDetailComponent, {
        panelClass: 'appointment-detail-dialog',
        width: '600px',
        data: {
          appointment: selectedAppointment,
          mode: 'view'
        }
    });
    appointmentDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadAppointmentList()
      }
    })
  }

  onClickCreate(): void {
    const appointmentDialog = this._matDialog.open(AppointmentDetailComponent, {
      panelClass: 'appointment-detail-dialog',
      width: '600px',
      data: {
        appointment: null,
        mode: 'create'
      }
    });
    appointmentDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadAppointmentList()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
