import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import { Appointment } from '../../model/appointment';
import { AppointmentService } from '../../service/appointment.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-appointment-detail',
  templateUrl: './appointment-detail.component.html',
  styleUrls: ['./appointment-detail.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AppointmentDetailComponent implements OnInit {
  dialogTitle = 'Appointment Detail';
  appointment: Appointment;
  mode: any;
  constructor(private _matDialogRef: MatDialogRef<AppointmentDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private appointmentService: AppointmentService,
              private toastService: ToastService) {
    this.appointment = _data.appointment || new Appointment();
    this.mode = _data.mode
  }

  ngOnInit(): void {
   // console.log(this.patient);
  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    this.appointmentService.updateAppointment(this.appointment._id, this.appointment)
      .subscribe((data: any) => {
        this.closeDialoag('appointmentSaved');
      }, response => {});
  }

  onClickCreate(): void {
    this.appointmentService.createAppointment(this.appointment)
      .subscribe((data: any) => {
        this.closeDialoag('appointmentCreated');
      }, response => {
        console.log(response);
      });
  }

  formValid(): boolean {
    return true;
  }

  toggleMode(): void {
    if (this.mode === 'view') {
      this.mode = 'update';
    } else {
      this.mode = 'view';
    }
  }
}
