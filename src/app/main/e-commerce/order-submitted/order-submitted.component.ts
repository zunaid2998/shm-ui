import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { OrderService } from '../service/order.service';
import { OrderAdminDetailComponent } from '../order-admin/order-admin-detail/order-admin-detail.component';
import { OrderSubmittedDetailComponent } from './order-submitted-detail/order-submitted-detail.component';

@Component({
  selector: 'app-order-submitted',
  templateUrl: './order-submitted.component.html',
  styleUrls: ['./order-submitted.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class OrderSubmittedComponent implements OnInit {
  orders: any[];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['orderNo', 'createdAt', 'status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private orderService: OrderService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.orders);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
  }

  ngOnInit(): void {
    this.loadOrders()
  }

  loadOrders(): void {
    this.loading = true;
    const userId = localStorage.getItem('userId')
    this.orderService.getOrdersByCustomer(userId)
      .subscribe((response: any) => {
        this.loading = false;
        this.orders = response.data;
        this.dataSource.data = this.orders;
      }, error => console.log(error));
  }

  onClickOrder(order: any): void {
    this._matDialog.open(OrderSubmittedDetailComponent, {
        panelClass: 'order-submitted-detail-dialog',
        width: '800px',
        height: '600px',
        data: {
          order
        },
        disableClose: true,
        autoFocus: false
    });
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
