import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-order-submitted-detail',
  templateUrl: './order-submitted-detail.component.html',
  styleUrls: ['./order-submitted-detail.component.scss']
})
export class OrderSubmittedDetailComponent implements OnInit {
  dialogTitle = 'Order Detail';
  order: any;
  displayedColumns: string[] = ['materialNo', 'description', 'quantity', 'allocated', 'unitOfMeasure'];
  dataSource: any[] = []
  constructor(private _matDialogRef: MatDialogRef<OrderSubmittedDetailComponent>,
              @Inject(MAT_DIALOG_DATA) _data: any) {
    this.order = _data.order
    this.dataSource = this.order.materials
  }

  ngOnInit(): void {}

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onSubmitOrder(): void {
    this.closeDialoag()
  }
}
