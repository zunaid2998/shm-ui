import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { OrderService } from '../../service/order.service';
import { Angular2Txt } from 'angular2-txt/Angular2-txt';
import { ToastService } from '../../service/toast.service';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';


@Component({
  selector: 'app-order-admin-detail',
  templateUrl: './order-admin-detail.component.html',
  styleUrls: ['./order-admin-detail.component.scss']
})

export class OrderAdminDetailComponent implements OnInit {
  dialogTitle = 'Order Detail';
  order: any;
  displayedColumns: string[] = ['materialNo', 'description', 'quantity', 'allocated', 'unitOfMeasure'];
  dataSource: any[] = []
  constructor(private _matDialogRef: MatDialogRef<OrderAdminDetailComponent>,
              private _matDialog: MatDialog,
              @Inject(MAT_DIALOG_DATA) _data: any,
              private orderService: OrderService,
              private toastService: ToastService) {
    this.order = _data.order
    this.dataSource = this.order.materials
  }

  ngOnInit(): void {}

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onSubmitOrder(): void {
    this.orderService.updateOrder(this.order._id, this.order)
      .subscribe(response => {
        this.closeDialoag()
      })
  }

  disableOrderStatus(): boolean {
    let disabled = true
    const materials = this.order.materials.filter(material => material.allocated && material.allocated > 0)
    if(materials.length ===  this.order.materials.length){
      disabled = false
    }
    return disabled
  }

  disableDone(): boolean {
    return this.disableOrderStatus()
  }

  onClickDownloadOrder(): void {
    this.orderService.downloadOrder(this.order.orderNo).subscribe(response => {
      if(response && response.data) {
        const createdAt = response.data.createdAt
        const order = response.data.order
        const orders = response.data.orders

        const orderCreatedAt = new Date(createdAt)
        const fullYear = orderCreatedAt.getFullYear()
        const month = ((orderCreatedAt.getMonth() + 1) < 10 ? '0' : '') + (orderCreatedAt.getMonth() + 1)
        const day = (orderCreatedAt.getDate() < 10 ? '0' : '') + orderCreatedAt.getDate();
        const hour = (orderCreatedAt.getHours() < 10 ? '0' : '') + orderCreatedAt.getHours();
        const minute = (orderCreatedAt.getMinutes() < 10 ? '0' : '') + orderCreatedAt.getMinutes();
        const second = (orderCreatedAt.getSeconds() < 10 ? '0' : '') + orderCreatedAt.getSeconds();
        const fileName = `${order.orderNo}_${fullYear}${month}${day}_${hour}${minute}${second}`

        const csvOptions = {
          fileName: fileName,
          fieldSeparator: '|',
          quoteStrings: '',
          decimalseparator: '.',
          showLabels: false,
          showTitle: false,
          title: '',
          useBom: false,
          noDownload: false,
        };

        new Angular2Txt(orders, fileName, csvOptions);
      }
    })
  }

  onClickDeleteOrder(): void {
    const confirmDialog = this._matDialog.open(FuseConfirmDialogComponent);
    confirmDialog.afterClosed().subscribe(data => {
      if (data) {
        this.orderService.deleteOrder(this.order._id)
          .subscribe(response => {
            if(response && response.message) {
              this.toastService.show(response.message);
            }
            this.closeDialoag('loadOrders');
          }, error => console.log(`order deletion failed with order id ${this.order._id}`))
      }
    })
  }

  onFocusOutAllocated(event: any): void {
    if(event.target.value === "0") {
      this.toastService.show('0 is not a valid amount')
      event.target.value = null
    }
  }

  private setAllocated(): void {
    this.order.materials = this.order.materials.map(material => {
      material.allocated = null
      return material
    })
  }
}
