import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { EventService } from '../service/event.service';
import { fuseAnimations } from '@fuse/animations';
import { OrderService } from '../service/order.service';
import { OrderAdminDetailComponent } from './order-admin-detail/order-admin-detail.component';

@Component({
  selector: 'app-order-admin',
  templateUrl: './order-admin.component.html',
  styleUrls: ['./order-admin.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class OrderAdminComponent implements OnInit {
  orders: any[];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['orderNo', 'customerName', 'address.addressName', 'createdAt', 'status', 'extracted'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  sortingDataAccessor(item, property) {
   if (property.includes('.')) { 
     return property.split('.')
       .reduce((object, key) => object[key], item);
   }
   return item[property];
 }

  constructor(private orderService: OrderService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.orders);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sortingDataAccessor = this.sortingDataAccessor
        this.dataSource.sort = this.sort;
      })
  }

  ngOnInit(): void {
    this.loadOrders()
  }

  loadOrders(): void {
    this.loading = true;
    this.orderService.getOrders()
      .subscribe((response: any) => {
        this.loading = false;
        this.orders = response.data;
        this.dataSource.data = this.orders;
      }, error => console.log(error));
  }

  onClickOrder(order: any): void {
    const orderAdminDetailPopup = this._matDialog.open(OrderAdminDetailComponent, {
        panelClass: 'order-admin-detail-dialog',
        width: '800px',
        height: '750px',
        data: {
          order
        },
        autoFocus: false
    });
    orderAdminDetailPopup.afterClosed().subscribe(data => {
      if(data) {
        this.loadOrders();
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
