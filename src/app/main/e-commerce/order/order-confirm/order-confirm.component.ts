import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MaterialService } from '../../service/material.service';
import { OrderService } from '../../service/order.service';
import { UserService } from '../../service/user.service';
import { SettingsService } from '../../service/settings.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-order-confirm',
  templateUrl: './order-confirm.component.html',
  styleUrls: ['./order-confirm.component.scss']
})
export class OrderConfirmComponent implements OnInit {
  dialogTitle = 'Confirm and Submit';
  materials: any[] = [];
  mode: any
  customerId: string
  customerName: string
  email: string
  user: any
  newAddress: any
  minDate: any = new Date()
  saveThisAddress: boolean
  selectedAddress: any
  hideSaveThisAddress: boolean
  displayedColumns: string[] = ['materialNo', 'description', 'quantity', 'unitOfMeasure'];
  dataSource: any[] = []
  orderRequiredDate: any
  orderPriority: string = ''
  orderPriorities: string[] = ['Routine', 'Urgent', 'Critical']
  facilityType: string = ''
  facilityTypes: string[] = []
  facilityName: string
  deliveryInstructions: string
  postalCodePattern = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/
  minOneCharacter = /[a-zA-Z0-9_]+.*$/
  submittingOrder = false
  constructor(private _matDialogRef: MatDialogRef<OrderConfirmComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private userService: UserService,
              private orderService: OrderService,
              private settingsService: SettingsService,
              private toastService: ToastService) {
    this.materials = _data.selectedMaterials || [];
    this.dataSource = this.materials
    this.user = _data.user || {}
    this.newAddress = {
      addressName: '',
      addressLine1: '',
      city: '',
      province: '',
      postalCode: ''
    }
    this.setCurrentOrderFromLocalStorage()
  }

  ngOnInit(): void {
   this.customerId = localStorage.getItem('userId')
   this.customerName = localStorage.getItem('userName')
   this.email = localStorage.getItem('email')
   this.loadSettings()
  }

  loadSettings(): void {
    this.settingsService.getSettings()
      .subscribe(response => {
        this.facilityTypes = response.data.facilityTypes
      })
  }

  onChangeSelectedAddress(): void {
    this.newAddress = this.selectedAddress
    this.hideSaveThisAddress = true
  }

  onClickCancelPreviousAddress(): void {
    this.selectedAddress = null
    this.newAddress = {
      addressName: '',
      addressLine1: '',
      city: '',
      province: '',
      postalCode: ''
    }
    this.hideSaveThisAddress = false 
    this.saveThisAddress = false
  }

  onChangeSelectedDate(): void {

  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  private getFullAddress(address: any): string {
    let fullAddress = `${address.addressLine1}, ${address.city}, ${address.province}, ${address.postalCode}`
    return fullAddress
  }

  onClickSubmitOrder(): void {
    this.submittingOrder = true;
    const order: any = {
      address: this.newAddress,
      materials: this.materials,
      email: this.email,
      customerId: this.customerId,
      customerName: this.customerName,
      status: 'submitted',
      dateRequired: this.orderRequiredDate,
      orderPriority: this.orderPriority,
      facilityType: this.facilityType,
      deliveryInstructions: this.deliveryInstructions
    }

    if(this.saveThisAddress){
      this.user.address.push(this.newAddress)
      this.userService.updateUser(this.user._id, this.user)
      .subscribe((response: any) => {
        this.user = response.data
      }, error => console.log(error))
    }

    this.orderService.createOrder(order)
      .subscribe((data: any) => {
        localStorage.setItem('currentOrder', null)
        this.closeDialoag('orderCreated');
      }, error => console.log(error));
  }

  onClickAdjust(): void {
    this.saveCurrentOrderToLocalStorage()
    this.closeDialoag()
  }

  formValid(): boolean {
    const {addressLine1, city, province, postalCode, addressName} = this.newAddress
    return addressName.length > 0 && addressLine1.length > 0 && city.length > 0 && province.length > 0 && postalCode.length > 0
           && this.orderRequiredDate && this.orderPriority.length > 0 && this.facilityType.length > 0
           && this.postalCodePattern.test(postalCode)
           && this.minOneCharacter.test(addressName)
           && this.minOneCharacter.test(addressLine1)
           && this.minOneCharacter.test(city)
           && this.minOneCharacter.test(province)            
  }

  onFocusOutField(event: any): void {
    const formValue = event.target.value
    if(formValue.length > 0 && !this.minOneCharacter.test(formValue)) {
      this.toastService.show('Must have atleast 1 character')
    }
  }

  onFocusOutPostalCode(event: any): void {
    const formValue = event.target.value
    if(formValue.length > 0 && !this.postalCodePattern.test(formValue)) {
      this.toastService.show('Must be a valid Postal Code')
    }
  }

  private setCurrentOrderFromLocalStorage(): void {
    const currentOrder = localStorage.getItem('currentOrder')
    if(!currentOrder) return
    const order = JSON.parse(currentOrder)
    if(!order) return
    this.newAddress = order.address
    this.orderRequiredDate = order.dateRequired
    this.orderPriority = order.orderPriority
    this.facilityType = order.facilityType
    this.deliveryInstructions = order.deliveryInstructions
    if(!order.selectedAddress){
      this.saveThisAddress = order.saveThisAddress
      this.hideSaveThisAddress = false
    }
    
    if(order.selectedAddress){
      this.selectedAddress = order.selectedAddress
      this.hideSaveThisAddress = true
    }
  }

  private saveCurrentOrderToLocalStorage(): void {
    const currentOrder: any = {
      address: this.newAddress,
      dateRequired: this.orderRequiredDate,
      orderPriority: this.orderPriority,
      facilityType: this.facilityType,
      deliveryInstructions: this.deliveryInstructions,
      selectedAddress: this.selectedAddress,
      saveThisAddress: this.saveThisAddress
    }
    localStorage.setItem('currentOrder', JSON.stringify(currentOrder))
  }

}
