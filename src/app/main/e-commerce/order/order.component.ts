import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { MaterialService } from '../service/material.service';
import { Material } from '../model/material.model';
import * as _ from 'lodash';
import { MaterialDetailComponent } from '../material/material-detail/material-detail.component';
import { fuseAnimations } from '@fuse/animations';
import { OrderedItem } from '../model/order.model';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { OrderConfirmComponent } from './order-confirm/order-confirm.component';
import { ToastService } from '../service/toast.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class OrderComponent implements OnInit {
  materials: any[];
  loading: boolean;
  // checkboxes: {};
  orders: any[]
  selectedIds: any[] = []
  address: string = ''
  user: any
  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['number', 'description', 'quantity', 'unitOfMeasure'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private materialService: MaterialService,
              private _matDialog: MatDialog,
              private toastService: ToastService,
              private userService: UserService) {
      this.dataSource = new MatTableDataSource(this.materials);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadMaterials()
    this.loadUser()
  }

  loadUser(): void {
    const userId = localStorage.getItem('userId')
    this.userService.getUser(userId)
      .subscribe((response: any) => {
        this.user = response.data
      })
  }

  loadMaterials(): void {
    this.loading = true;
    this.materialService.getActiveMaterials()
      .subscribe((response: any) => {
        this.loading = false;
        const mats: any[] = response.data
        this.materials = mats.map(mat => {
          const material: any  = {
            id: mat._id,
            number: mat.number,
            description: mat.description,
            quantity: null,
            unitOfMeasure: mat.unitOfMeasure
          }
          return material
        })
        this.dataSource.data = this.materials;
      }, error => console.log(error));
  }

  onClickMaterialDetail(material: any): void {
    let selectedMaterial: Material = new Material();
    selectedMaterial = _.clone(material);
    const materialDialog = this._matDialog.open(MaterialDetailComponent, {
        panelClass: 'material-detail-dialog',
        width: '600px',
        data: {
          material: selectedMaterial,
          mode: 'view'
        }
    });
    materialDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadMaterials()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  onClickConfirm(): void {
    const selectedMaterials = this.materials.filter(material => material.quantity > 0)
    if(selectedMaterials.length === 0){
      this.toastService.show('Please select atleast one product with valid quantity', 3000)
      return
    }
    const orderConfirmDialog = this._matDialog.open(OrderConfirmComponent, {
      panelClass: 'order-confirm-dialog',
      width: '800px',
      height: '750px',
      data: {
        selectedMaterials: selectedMaterials,
        user: this.user
      },
      disableClose: true,
      autoFocus: false
    });
    orderConfirmDialog.afterClosed().subscribe(data => {
      if(data === 'orderCreated') {
       this.toastService.show('Congratulations! Your order has been submitted.', 5000)
       this.loadMaterials()
      }
    }) 
  }

  onFocusOutQuantity(event: any): void {
    if(event.target.value === "0") {
      this.toastService.show('0 is not a valid amount')
      event.target.value = null
    }
  }

}
