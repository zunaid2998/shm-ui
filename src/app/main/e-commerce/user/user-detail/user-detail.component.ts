import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../service/user.service';
import { User } from '../../model/user.model';
import { ToastService } from '../../service/toast.service';
import { AbstractControl, ValidationErrors } from '@angular/forms';
import { AuthService } from '../../service/auth.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.scss']
})
export class UserDetailComponent implements OnInit {
  dialogTitle = 'User Detail';
  user: any;
  mode: any
  resetPassword: string = ''
  isAdmin: boolean = false
  constructor(private _matDialogRef: MatDialogRef<UserDetailComponent>,
              @Inject(MAT_DIALOG_DATA) _data: any,
              private userService: UserService,
              private toastService: ToastService,
              private authService: AuthService) {
    this.user = _data.user || {};
    this.isAdmin = this.user.userType === 'admin'
    this.mode = _data.mode
  }

  ngOnInit(): void {}

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    if(this.isAdmin) {
      this.user.userType = 'admin'
    } else {
      this.user.userType = 'customer'
    }

    this.userService.updateUser(this.user._id, this.user)
      .subscribe(() => {
        this.closeDialoag('userClosed');
      }, (response: any) => this.toastService.show(response.error.msg));
  }

  onClickCreate(): void {
    if(this.isAdmin) {
      this.user.userType = 'admin'
    } else {
      this.user.userType = 'customer'
    }
    
    this.user.email = this.user.email.toLowerCase()
    this.userService.createUser(this.user)
      .subscribe(response => {
        if(!response.data) {
          this.toastService.show(response.message)
          return
        }
        this.user = response.data
        this.authService.sendResetPasswordLink(this.user.email)
          .subscribe(response => {
            if(response.data) {
              this.toastService.show(`A reset password link has been sent to ${this.user.email}. The link expires in 24 hours.`)
            }
            this.closeDialoag('userClosed');
          })
        
      }, (response: any) => this.toastService.show(response.error.msg));
  }

  formValid(): boolean {
    return this.user.email && this.user.firstName && this.user.lastName
  }
}
