import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { UserService } from '../service/user.service';
import { User } from '../model/user.model';
import { UserDetailComponent } from './user-detail/user-detail.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class UserComponent implements OnInit {
  users: any[];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'email', 'userType'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private userService: UserService,
    private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.users);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });   
  }

  ngOnInit(): void {
    this.loadUsers()
  }

  loadUsers(): void {
    this.loading = true;
    this.userService.getUsers()
      .subscribe((response: any) => {
        this.loading = false;
        const users: any[] = response.data;
        this.users = users.map(user => { 
          user.name = user.firstName + ' ' + user.lastName
          return user
        })
        this.dataSource.data = this.users;
      }, error => console.log(error));
  }

  onClickUserDetail(user: any): void {
    let selectedUser: User = new User();
    selectedUser = _.clone(user);
    const userDialog = this._matDialog.open(UserDetailComponent, {
        panelClass: 'user-detail-dialog',
        width: '600px',
        data: {
          user: selectedUser,
          mode: 'update'
        }
    });
    userDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadUsers()
      }
    })
  }

  onClickCreateUser(): void {
    const userDialog = this._matDialog.open(UserDetailComponent, {
      panelClass: 'user-detail-dialog',
      width: '600px',
      data: {
        user: null,
        mode: 'create'
      }
    });
    userDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadUsers()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getUserName(user: any): string {
    return `${user.firstName} ${user.lastName}`
  }
}
