import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { Station } from '../model/station.model';
import { StationService } from '../service/station.service';
import { StationDetailComponent } from './station-detail/station-detail.component';

@Component({
  selector: 'app-station',
  templateUrl: './station.component.html',
  styleUrls: ['./station.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class StationComponent implements OnInit {
  stations: any[] = [];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['stationNumber', 'stationName', 'businessStartTime', 'businessEndTime', 'city', 'phoneNumber'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private stationService: StationService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.stations);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadStations()
  }

  loadStations(): void {
    this.loading = true;
    this.stationService.getStations()
      .subscribe((response: any) => {
        this.loading = false;
        this.stations = response.data;
        this.dataSource.data = this.stations;
      }, error => console.log(error));
  }

  onClickStationDetail(station: any): void {
    let selectedStation: Station = new Station();
    selectedStation = _.clone(station);
    const stationDialog = this._matDialog.open(StationDetailComponent, {
        panelClass: 'station-detail-dialog',
        width: '600px',
        data: {
          station: selectedStation,
          mode: 'view'
        }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadStations()
      }
    })
  }

  onClickCreateStation(): void {
    const stationDialog = this._matDialog.open(StationDetailComponent, {
      panelClass: 'station-detail-dialog',
      width: '600px',
      data: {
        station: null,
        mode: 'create'
      }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadStations()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
