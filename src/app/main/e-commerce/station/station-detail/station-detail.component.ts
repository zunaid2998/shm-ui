import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Station } from '../../model/station.model';
import { StationService } from '../../service/station.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-station-detail',
  templateUrl: './station-detail.component.html',
  styleUrls: ['./station-detail.component.scss']
})
export class StationDetailComponent implements OnInit {
  dialogTitle = 'Station Detail';
  station: Station;
  mode: any
  constructor(private _matDialogRef: MatDialogRef<StationDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private stationService: StationService,
              private toastService: ToastService) {
    this.station = _data.station || new Station();
    this.mode = _data.mode
  }

  ngOnInit(): void {
   // console.log(this.patient);
  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    this.stationService.updateStation(this.station._id, this.station)
      .subscribe((data: any) => {
        this.closeDialoag('stationSaved');
      }, response => {
        if(response.error.codeName === 'DuplicateKey') this.toastService.show('Duplicate Station No. found. Station No. needs to be unique')
      });
  }

  onClickCreate(): void {
    this.stationService.createStation(this.station)
      .subscribe((data: any) => {
        this.closeDialoag('stationSaved');
      }, response => {
        if(response.error.codeName === 'DuplicateKey') this.toastService.show('Duplicate Station No. found. Station No. needs to be unique')
      });
  }

  formValid(): boolean {
    return true;
  }

  toggleMode(): void {
    if (this.mode === 'view') {
      this.mode = 'update';
    } else {
      this.mode = 'view';
    }
  }

}
