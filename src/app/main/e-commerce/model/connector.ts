export class Connector {
  _id: string;
  connectorNumber: number;
  type: string;
  power: number;
  status: string;
}