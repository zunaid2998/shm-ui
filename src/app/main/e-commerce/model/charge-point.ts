export class ChargePoint {
  _id: string;
  chargePointNumber: number;
  chargePointModel: string;
  serialNumber: string;
  meterSerialNumber: string;
  meterType: string;
  vendor: string;
  ocppFwVersion: string;
  seccFwVersion: string;
  iccid: string;
  imsi: string;
  network: string;
  timezone: string;
  lastConnectTime: string;
  onlineTime: string;
  note: string;
  stationId: string;
  stationName: string;
}