export interface OrderedItem {
  materialId: any
  materialDescription: any
  quantity: number
}