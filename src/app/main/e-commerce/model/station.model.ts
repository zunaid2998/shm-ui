export class Station {
  _id: string;
  stationNumber: number;
  stationName: string;
  businessStartTime: string
  businessEndTime: string;
  phoneNumber: string;
  city: string;
  picture: string;
  chargingStrategy: {
    ac: string;
    dc: string;
  }
  address: {
    lat: string;
    lng: string;
    fullAddress: string;
  }
}