export class PaymentTransaction {
  _id: string;
  userId: string;
  appointmentId: string;
  stationId: string;
  amount: number;
  status: string;
  stripeToken: string;
  createdAt: string;
  updatedAt: string;
}