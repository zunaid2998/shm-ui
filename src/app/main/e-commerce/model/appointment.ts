export class Appointment {
  _id: string;
  appointmentNo: string;
  startTime: string;
  endTime: string;
  estimatedCost: number;
  actualCost: number;
  status: string;
  userId: string;
  vehicleId: string;
  connectorId: string;
  stationId: string;
  createdAt: string;
  updatedAt: string;
}