import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { ChargingStrategy } from '../model/charging-strategy.model';
import { ChargingStrategyService } from '../service/charging-strategy.service';
import { ChargingStrategyDetailComponent } from './charging-strategy-detail/charging-strategy-detail.component';

@Component({
  selector: 'app-charging-strategy',
  templateUrl: './charging-strategy.component.html',
  styleUrls: ['./charging-strategy.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ChargingStrategyComponent implements OnInit {
  chargingStrategyList: any[] = [];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['strategyName'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private chargingStrategyService: ChargingStrategyService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.chargingStrategyList);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadChargingStrategyList()
  }

  loadChargingStrategyList(): void {
    this.loading = true;
    this.chargingStrategyService.getChargingStrategy()
      .subscribe((response: any) => {
        this.loading = false;
        this.chargingStrategyList = response.data;
        this.dataSource.data = this.chargingStrategyList;
      }, error => console.log(error));
  }

  onClickDetail(chargingStrategy: any): void {
    let selectedStrategy: ChargingStrategy = new ChargingStrategy();
    selectedStrategy = _.clone(chargingStrategy);
    const stationDialog = this._matDialog.open(ChargingStrategyDetailComponent, {
        panelClass: 'strategy-detail-dialog',
        width: '600px',
        data: {
          strategy: selectedStrategy,
          mode: 'view'
        }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadChargingStrategyList()
      }
    })
  }

  onClickCreate(): void {
    const stationDialog = this._matDialog.open(ChargingStrategyDetailComponent, {
      panelClass: 'strategy-detail-dialog',
      width: '600px',
      data: {
        strategy: null,
        mode: 'create'
      }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadChargingStrategyList()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
