import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChargingStrategy } from '../../model/charging-strategy.model';
import { ChargingStrategyService } from '../../service/charging-strategy.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-charging-strategy-detail',
  templateUrl: './charging-strategy-detail.component.html',
  styleUrls: ['./charging-strategy-detail.component.scss']
})
export class ChargingStrategyDetailComponent implements OnInit {
  dialogTitle = 'Charging Strategy Detail';
  strategy: ChargingStrategy;
  mode: any
  constructor(private _matDialogRef: MatDialogRef<ChargingStrategyDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private strategyService: ChargingStrategyService,
              private toastService: ToastService) {
    this.strategy = _data.strategy || new ChargingStrategy();
    this.mode = _data.mode
  }

  ngOnInit(): void {
   // console.log(this.patient);
  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    this.strategyService.updateChargingStrategy(this.strategy._id, this.strategy)
      .subscribe((data: any) => {
        this.closeDialoag('strategySaved');
      }, response => {});
  }

  onClickCreate(): void {
    this.strategyService.createChargingStrategy(this.strategy)
      .subscribe((data: any) => {
        this.closeDialoag('stationSaved');
      }, response => {});
  }

  formValid(): boolean {
    return true;
  }

  toggleMode(): void {
    if (this.mode === 'view') {
      this.mode = 'update';
    } else {
      this.mode = 'view';
    }
  }
}
