import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { ChargePoint } from '../model/charge-point';
import { ChargePointService } from '../service/charge-point.service';
import { ChargePointDetailComponent } from './charge-point-detail/charge-point-detail.component';

@Component({
  selector: 'app-charge-point',
  templateUrl: './charge-point.component.html',
  styleUrls: ['./charge-point.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ChargePointComponent implements OnInit {
  chargePointList: any[] = [];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['chargePointNumber', 'chargePointModel', 'serialNumber', 'meterSerialNumber', 'meterType', 'vendor', 'ocppFwVersion', 'seccFwVersion', 'iccid', 'imsi', 'network', 'timezone', 'lastConnectTime', 'onlineTime', 'note'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private chargePointService: ChargePointService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.chargePointList);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadchargePointList()
  }

  loadchargePointList(): void {
    this.loading = true;
    this.chargePointService.getChargePoint()
      .subscribe((response: any) => {
        this.loading = false;
        this.chargePointList = response.data;
        this.dataSource.data = this.chargePointList;
      }, error => console.log(error));
  }

  onClickDetail(chargePoint: any): void {
    let selectedChargePoint: ChargePoint = new ChargePoint();
    selectedChargePoint = _.clone(chargePoint);
    const stationDialog = this._matDialog.open(ChargePointDetailComponent, {
        panelClass: 'charge-point-detail-dialog',
        width: '600px',
        data: {
          chargePoint: selectedChargePoint,
          mode: 'view'
        }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadchargePointList()
      }
    })
  }

  onClickCreate(): void {
    const stationDialog = this._matDialog.open(ChargePointDetailComponent, {
      panelClass: 'charge-point-detail-dialog',
      width: '600px',
      data: {
        chargePoint: null,
        mode: 'create'
      }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadchargePointList()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
