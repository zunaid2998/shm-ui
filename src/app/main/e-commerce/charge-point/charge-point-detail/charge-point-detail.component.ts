import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ChargePoint } from '../../model/charge-point';
import { ChargePointService } from '../../service/charge-point.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-charge-point-detail',
  templateUrl: './charge-point-detail.component.html',
  styleUrls: ['./charge-point-detail.component.scss']
})
export class ChargePointDetailComponent implements OnInit {
  dialogTitle = 'Charge Point Detail';
  chargePoint: ChargePoint;
  mode: any
  constructor(private _matDialogRef: MatDialogRef<ChargePointDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private chargePointService: ChargePointService,
              private toastService: ToastService) {
    this.chargePoint = _data.chargePoint || new ChargePoint();
    this.mode = _data.mode
  }

  ngOnInit(): void {
   // console.log(this.patient);
  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    this.chargePointService.updateChargePoint(this.chargePoint._id, this.chargePoint)
      .subscribe((data: any) => {
        this.closeDialoag('chargePointSaved');
      }, response => {});
  }

  onClickCreate(): void {
    this.chargePointService.createChargePoint(this.chargePoint)
      .subscribe((data: any) => {
        this.closeDialoag('chargePointSaved');
      }, response => {});
  }

  formValid(): boolean {
    return true;
  }

  toggleMode(): void {
    if (this.mode === 'view') {
      this.mode = 'update';
    } else {
      this.mode = 'view';
    }
  }
}
