import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { EventService } from '../service/event.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import * as _ from 'lodash';
import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EventComponent implements OnInit {
  events: any[];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['name', 'event', 'time'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private eventService: EventService) {
      this.dataSource = new MatTableDataSource(this.events);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }) 
  }

  ngOnInit(): void {
    this.loadEvents()
  }

  loadEvents(): void {
    this.loading = true;
    this.eventService.getEvents()
      .subscribe((response: any) => {
        this.loading = false;
        this.events = response.data;
        this.dataSource.data = this.events;
      }, error => console.log(error));
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
