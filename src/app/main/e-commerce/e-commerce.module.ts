import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
    MatButtonModule, MatChipsModule, MatExpansionModule, MatFormFieldModule, MatIconModule, MatInputModule, MatPaginatorModule, MatRippleModule, MatSelectModule, MatSnackBarModule,
    MatSortModule,
    MatTableModule, MatTabsModule, MatCardModule, MatDialogModule, MatListModule, MatToolbarModule, MatCheckboxModule, MatRadioModule, MatDatepickerModule
} from '@angular/material';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';
import { UserService } from './service/user.service';
import { MaterialService } from './service/material.service';
import { MaterialComponent } from './material/material.component';
import { MaterialDetailComponent } from './material/material-detail/material-detail.component';
import { UserComponent } from './user/user.component';
import { UserDetailComponent } from './user/user-detail/user-detail.component';
import { OrderComponent } from './order/order.component';
import { OrderConfirmComponent } from './order/order-confirm/order-confirm.component';
import { OrderService } from './service/order.service';
import { AuthGuard } from './service/auth.guard';
import { AdminGuard } from './service/admin.guard';
import { CustomerGuard } from './service/customer.guard';
import { EventComponent } from './event/event.component';
import { EventService } from './service/event.service';
import { OrderAdminComponent } from './order-admin/order-admin.component';
import { OrderAdminDetailComponent } from './order-admin/order-admin-detail/order-admin-detail.component';
import { SettingsComponent } from './settings/settings.component';
import { SettingsService } from './service/settings.service';
import { OrderSubmittedComponent } from './order-submitted/order-submitted.component';
import { OrderSubmittedDetailComponent } from './order-submitted/order-submitted-detail/order-submitted-detail.component';
import { AddressDetailComponent } from './settings/address-detail/address-detail.component';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { FuseConfirmDialogModule } from '@fuse/components';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { StationComponent } from './station/station.component';
import { StationService } from './service/station.service';
import { StationDetailComponent } from './station/station-detail/station-detail.component';
import { ChargingStrategyComponent } from './charging-strategy/charging-strategy.component';
import { ChargingStrategyDetailComponent } from './charging-strategy/charging-strategy-detail/charging-strategy-detail.component';
import { ChargingStrategyService } from './service/charging-strategy.service';
import { ChargePointComponent } from './charge-point/charge-point.component';
import { ChargePointDetailComponent } from './charge-point/charge-point-detail/charge-point-detail.component';
import { ChargePointService } from './service/charge-point.service';
import { ConnectorComponent } from './connector/connector.component';
import { ConnectorDetailComponent } from './connector/connector-detail/connector-detail.component';
import { ConnectorService } from './service/connector.service';
import { AppointmentComponent } from './appointment/appointment.component';
import { AppointmentDetailComponent } from './appointment/appointment-detail/appointment-detail.component';
import { PaymentTransactionComponent } from './payment-transaction/payment-transaction.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardService } from './service/dashboard.service';
import { AppointmentService } from './service/appointment.service';
import { PaymentTransactionService } from './service/payment-transaction.service';

const maskConfig: Partial<IConfig> = {};

const routes: Routes = [
    {
        path     : 'materials',
        component: MaterialComponent,
        canActivate: [AuthGuard, AdminGuard]
    },
    {
        path     : 'users',
        component: UserComponent,
        canActivate: [AuthGuard, AdminGuard]
    },
    {
        path     : 'orders',
        component: OrderComponent,
        canActivate: [AuthGuard, CustomerGuard]
    },
    {
        path     : 'orders-submitted',
        component: OrderSubmittedComponent,
        canActivate: [AuthGuard, CustomerGuard]
    },
    {
        path     : 'events',
        component: EventComponent,
        canActivate: [AuthGuard, AdminGuard]
    },
    {
        path     : 'orders-admin',
        component: OrderAdminComponent,
        canActivate: [AuthGuard, AdminGuard]
    },
    {
        path     : 'settings',
        component: SettingsComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'station',
        component: StationComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'strategy',
        component: ChargingStrategyComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'charge-point',
        component: ChargePointComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'connector',
        component: ConnectorComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'dashboard',
        component: DashboardComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'appointment',
        component: AppointmentComponent,
        canActivate: [AuthGuard]
    },
    {
        path     : 'payment',
        component: PaymentTransactionComponent,
        canActivate: [AuthGuard]
    },
];

@NgModule({
    declarations: [
        MaterialComponent,
        MaterialDetailComponent,
        UserComponent,
        UserDetailComponent,
        OrderComponent,
        OrderConfirmComponent,
        EventComponent,
        OrderAdminComponent,
        OrderAdminDetailComponent,
        SettingsComponent,
        OrderSubmittedComponent,
        OrderSubmittedDetailComponent,
        AddressDetailComponent,
        StationComponent,
        StationDetailComponent,
        ChargingStrategyComponent,
        ChargingStrategyDetailComponent,
        ChargePointComponent,
        ChargePointDetailComponent,
        ConnectorComponent,
        ConnectorDetailComponent,
        AppointmentComponent,
        AppointmentDetailComponent,
        PaymentTransactionComponent,
        DashboardComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatChipsModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatPaginatorModule,
        MatRippleModule,
        MatSelectModule,
        MatSortModule,
        MatSnackBarModule,
        MatTableModule,
        MatTabsModule,

        NgxChartsModule,
        NgxMaskModule.forRoot(maskConfig),

        FuseSharedModule,
        FuseWidgetModule,
        MatCardModule,
        MatDialogModule,
        MatListModule,
        MatToolbarModule,
        MatCheckboxModule,
        MatTabsModule,
        MatRadioModule,
        MatDatepickerModule,
        FuseConfirmDialogModule
    ],
    providers   : [
        UserService,
        MaterialService,
        OrderService,
        EventService,
        SettingsService,
        StationService,
        ChargingStrategyService,
        ChargePointService,
        ConnectorService,
        DashboardService,
        AppointmentService,
        PaymentTransactionService
    ],
    entryComponents: [
        MaterialDetailComponent,
        UserDetailComponent,
        OrderConfirmComponent,
        OrderAdminDetailComponent,
        OrderSubmittedDetailComponent,
        AddressDetailComponent,
        FuseConfirmDialogComponent,
        StationDetailComponent,
        ChargingStrategyDetailComponent,
        ChargePointDetailComponent,
        ConnectorDetailComponent,
        AppointmentDetailComponent
    ]
})
export class EcommerceModule
{
}
