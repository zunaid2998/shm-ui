import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Connector } from '../../model/connector';
import { ConnectorService } from '../../service/connector.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-connector-detail',
  templateUrl: './connector-detail.component.html',
  styleUrls: ['./connector-detail.component.scss']
})
export class ConnectorDetailComponent implements OnInit {
  dialogTitle = 'Connector Detail';
  connector: Connector;
  mode: any
  constructor(private _matDialogRef: MatDialogRef<ConnectorDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private connectorService: ConnectorService,
              private toastService: ToastService) {
    this.connector = _data.connector || new Connector();
    this.mode = _data.mode
  }

  ngOnInit(): void {
   // console.log(this.patient);
  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    this.connectorService.updateConnector(this.connector._id, this.connector)
      .subscribe((data: any) => {
        this.closeDialoag('connectorSaved');
      }, response => {});
  }

  onClickCreate(): void {
    this.connectorService.createConnector(this.connector)
      .subscribe((data: any) => {
        this.closeDialoag('connectorSaved');
      }, response => {
        console.log(response);
      });
  }

  formValid(): boolean {
    return true;
  }

  toggleMode(): void {
    if (this.mode === 'view') {
      this.mode = 'update';
    } else {
      this.mode = 'view';
    }
  }
}
