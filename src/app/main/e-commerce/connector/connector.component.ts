import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { Connector } from '../model/connector';
import { ConnectorService } from '../service/connector.service';
import { ConnectorDetailComponent } from './connector-detail/connector-detail.component';

@Component({
  selector: 'app-connector',
  templateUrl: './connector.component.html',
  styleUrls: ['./connector.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ConnectorComponent implements OnInit {
  connectorList: any[] = [];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['connectorNumber', 'type', 'power', 'status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private connectorService: ConnectorService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.connectorList);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadconnectorList()
  }

  loadconnectorList(): void {
    this.loading = true;
    this.connectorService.getConnector()
      .subscribe((response: any) => {
        this.loading = false;
        this.connectorList = response.data;
        this.dataSource.data = this.connectorList;
      }, error => console.log(error));
  }

  onClickDetail(connector: any): void {
    let selectedConnector: Connector = new Connector();
    selectedConnector = _.clone(connector);
    const stationDialog = this._matDialog.open(ConnectorDetailComponent, {
        panelClass: 'connector-detail-dialog',
        width: '600px',
        data: {
          connector: selectedConnector,
          mode: 'view'
        }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadconnectorList()
      }
    })
  }

  onClickCreate(): void {
    const stationDialog = this._matDialog.open(ConnectorDetailComponent, {
      panelClass: 'connector-detail-dialog',
      width: '600px',
      data: {
        connector: null,
        mode: 'create'
      }
    });
    stationDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadconnectorList()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
