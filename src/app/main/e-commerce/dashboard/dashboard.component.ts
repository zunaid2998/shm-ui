import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { DashboardService } from '../service/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [fuseAnimations],
  encapsulation: ViewEncapsulation.None
})
export class DashboardComponent implements OnInit {
  appointments: number;
  stations: number;
  users: number;
  loading: boolean;
  constructor(private _dashboardService: DashboardService) { }

  ngOnInit(): void {
    this.loading = true;
    this._dashboardService.getDashboardDetails()
      .subscribe((data: any) => {
        console.log(data);
        if (data){
          this.appointments = data.appointments;
          this.stations = data.stations;
          this.users = data.users;
          this.loading = false;
        }
      });
  }
}
