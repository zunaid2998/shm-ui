import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { SettingsService } from '../service/settings.service';
import { ToastService } from '../service/toast.service';
import { UserService } from '../service/user.service';
import { MatDialog, MatTableDataSource } from '@angular/material';
import { AddressDetailComponent } from './address-detail/address-detail.component';
import * as _ from 'lodash'

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class SettingsComponent implements OnInit {
  settings: any = {};
  loading: boolean;
  userType: string
  userId: string
  displayedColumns: string[] = ['addressName', 'fullAddress'];
  dataSource: any[] = []
  dataSourceFacilityTypes = new MatTableDataSource<any>()
  displayedColumnsFacilityTypes: string[] = ['facilityType', 'deleteIcon']
  user: any = {
    apiToken: ''
  }
  newFacilityType: string = ''

  constructor(private settingsService: SettingsService,
              private toastService: ToastService,
              private userService: UserService,
              private _matDialog: MatDialog) {
  }

  ngOnInit(): void {
    this.userType = localStorage.getItem('userType')
    this.userId = localStorage.getItem('userId')
    this.loadUserInfo()
    this.loadSettings()
  }

  loadUserInfo(): void {
    this.loading = true;
    this.userService.getUser(this.userId)
      .subscribe(response => {
        this.user = response.data
        this.dataSource = this.user.address
      }, error => console.log(error), () => this.loading = false)
  }

  onClickGenerateToken(): void {
    this.userService.generatApiToken(this.userId)
      .subscribe(response => {
        this.user = response.data
      })
  }

  loadSettings(): void {
    if(this.userType === 'customer') return
    this.loading = true;
    this.settingsService.getSettings()
      .subscribe((response: any) => {
        this.settings = response.data;
        this.dataSourceFacilityTypes.data = this.settings.facilityTypes
      }, error => console.log(error), () => this.loading = false);
  }

  onClickSave(): void {
    this.settingsService.updateSettings(this.settings)
      .subscribe((response: any) => {
        this.loading = false;
        this.settings = response.data;
        localStorage.setItem('sendOrderToEmail', this.settings.sendOrderToEmail)
        this.toastService.show('Settings Saved', 3000)
      }, error => console.log(error));
  }

  getFullAddress(address): string {
    if(address){
      return `${address.addressLine1}, ${address.city}, ${address.province}, ${address.postalCode}`
    }
  }

  onClickAddress(address: any, index: any): void {
    const selectedAddress = _.clone(address)
    const addressDetialDialog = this._matDialog.open(AddressDetailComponent, {
      panelClass: 'address-detail-dialog',
      width: '600px',
      data: {
        selectedAddress,
        index,
        mode: 'update',
        user: this.user
      },
      autoFocus: false
    })

    addressDetialDialog.afterClosed().subscribe(data => {
      if(data === 'addressSaved') {
       this.loadUserInfo()
      }
    })
  }

  onClickAddAddress(): void {
    const addressDetialDialog = this._matDialog.open(AddressDetailComponent, {
      panelClass: 'address-detail-dialog',
      width: '600px',
      data: {
        selectedAddress: null,
        mode: 'create',
        user: this.user
      },
      autoFocus: false
    })

    addressDetialDialog.afterClosed().subscribe(data => {
      if(data === 'addressSaved') {
       this.loadUserInfo()
      }
    })
  }

  onClickAddFacility(): void {
    this.settings.facilityTypes.push(this.newFacilityType)
    this.dataSourceFacilityTypes.data = this.settings.facilityTypes
    this.newFacilityType = ''
  }

  onClickDeleteFacility(index: any): void {
    this.settings.facilityTypes.splice(index, 1)
    this.dataSourceFacilityTypes.data = this.settings.facilityTypes
  }

}
