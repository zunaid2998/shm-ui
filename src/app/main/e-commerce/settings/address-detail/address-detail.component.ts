import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { UserService } from '../../service/user.service';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-address-detail',
  templateUrl: './address-detail.component.html',
  styleUrls: ['./address-detail.component.scss']
})
export class AddressDetailComponent implements OnInit {
  dialogTitle = 'Address Detail';
  selectedAddress: any
  user: any
  mode: any
  index: any
  address: any[] = []
  postalCodePattern = /^[A-Za-z]\d[A-Za-z][ -]?\d[A-Za-z]\d$/
  minOneCharacter = /[a-zA-Z0-9_]+.*$/
  constructor(private _matDialogRef: MatDialogRef<AddressDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private userService: UserService,
              private toastService: ToastService) {
    this.selectedAddress = _data.selectedAddress || {
      addressName: '',
      addressLine1: '',
      city: '',
      province: '',
      postalCode: ''
    };
    this.mode = _data.mode
    this.index = _data.index
    this.user = _data.user
  }

  ngOnInit(): void {}

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickAdd(): void {
    this.user.address.push(this.selectedAddress)
    this.userService.updateUser(this.user._id, this.user)
      .subscribe(response => {
        this.closeDialoag('addressSaved')
      })
  }

  onClickSave(): void {
    this.user.address[this.index] = this.selectedAddress
    this.userService.updateUser(this.user._id, this.user)
      .subscribe(response => {
        this.closeDialoag('addressSaved')
      })
  }

  formValid(): boolean {
    const {addressLine1, city, province, postalCode, addressName} = this.selectedAddress
    return (addressName && addressName.length) > 0 && addressLine1.length > 0 && city.length > 0 && province.length > 0 && postalCode.length > 0
            && this.postalCodePattern.test(postalCode)
            && this.minOneCharacter.test(addressName)
            && this.minOneCharacter.test(addressLine1)
            && this.minOneCharacter.test(city)
            && this.minOneCharacter.test(province)
  }

  onFocusOutField(event: any): void {
    const formValue = event.target.value
    if(formValue.length > 0 && !this.minOneCharacter.test(formValue)) {
      this.toastService.show('Must have atleast 1 character')
    }
  } 

  onFocusOutPostalCode(event: any): void {
    const formValue = event.target.value
    if(formValue.length > 0 && !this.postalCodePattern.test(formValue)) {
      this.toastService.show('Must be a valid Postal Code')
    }
  }
}
