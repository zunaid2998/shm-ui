import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { Material } from '../model/material.model';

@Injectable()
export class MaterialService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  // private updatedNurse = new BehaviorSubject<Nurse>(null);
  // updatedNurse$ = this.updatedNurse.asObservable();
  // updateNurse(nurse: Nurse): void {
  //   this.updatedNurse.next(nurse);
  // }

  constructor(private _httpClient: HttpClient) {}

  getMaterials(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/material`, {headers: this.reqHeader});
  }

  getActiveMaterials(): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/active/material`, {headers: this.reqHeader});
}

  saveMaterial(id: any, material: Material): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/material/${id}`, material, {headers: this.reqHeader});
  }

  createMaterial(material: Material): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/material`, material, {headers: this.reqHeader});
  }
}
