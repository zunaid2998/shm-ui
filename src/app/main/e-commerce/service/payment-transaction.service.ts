import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PaymentTransactionService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getPaymentTransaction(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/paymentTransaction`, {headers: this.reqHeader});
  }

  getPaymentTransactionById(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/paymentTransaction/${id}`, {headers: this.reqHeader});
  }

  updatePaymentTransaction(id: any, paymentTransaction: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/paymentTransaction/${id}`, paymentTransaction, {headers: this.reqHeader});
  }

  createPaymentTransaction(paymentTransaction: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/paymentTransaction`, paymentTransaction, {headers: this.reqHeader});
  }

  deletePaymentTransaction(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/paymentTransaction/${id}`, {headers: this.reqHeader});
  }

  getPaymentTransactionByUserId(userId: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/user/${userId}/paymentTransaction`, {headers: this.reqHeader});
  }

  getPaymentTransactionByStationId(stationId: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/station/${stationId}/paymentTransaction`, {headers: this.reqHeader});
  }
}
