import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChargingStrategyService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getChargingStrategy(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/chargingStrategy`, {headers: this.reqHeader});
  }

  getChargingStrategyById(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/chargingStrategy/${id}`, {headers: this.reqHeader});
  }

  updateChargingStrategy(id: any, chargingStrategy: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/chargingStrategy/${id}`, chargingStrategy, {headers: this.reqHeader});
  }

  createChargingStrategy(chargingStrategy: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/chargingStrategy`, chargingStrategy, {headers: this.reqHeader});
  }

  deleteChargingStrategy(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/chargingStrategy/${id}`, {headers: this.reqHeader});
  }
}
