import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppointmentService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getAppointment(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/appointment`, {headers: this.reqHeader});
  }

  getAppointmentById(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/appointment/${id}`, {headers: this.reqHeader});
  }

  updateAppointment(id: any, Appointment: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/appointment/${id}`, Appointment, {headers: this.reqHeader});
  }

  createAppointment(Appointment: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/appointment`, Appointment, {headers: this.reqHeader});
  }

  deleteAppointment(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/appointment/${id}`, {headers: this.reqHeader});
  }
}
