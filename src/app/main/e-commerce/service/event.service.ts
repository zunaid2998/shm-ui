import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable()
export class EventService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json'
  });

  constructor(private _httpClient: HttpClient) {}

  getEvents(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/event`, {headers: this.reqHeader});
  }

  createEvent(event: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/event`, event, {headers: this.reqHeader});
  }
}
