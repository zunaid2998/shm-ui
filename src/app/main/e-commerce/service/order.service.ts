import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable()
export class OrderService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getOrders(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/order`, {headers: this.reqHeader});
  }

  getOrdersByCustomer(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/user/${id}/order`, {headers: this.reqHeader});
}

  updateOrder(id: any, order: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/order/${id}`, order, {headers: this.reqHeader});
  }

  createOrder(order: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/order`, order, {headers: this.reqHeader});
  }

  downloadOrder(orderNo: any): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/order/${orderNo}/download`, {headers: this.reqHeader});
  }

  deleteOrder(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/order/${id}`, {headers: this.reqHeader});
  }
}
