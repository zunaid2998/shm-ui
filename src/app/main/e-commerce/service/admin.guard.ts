import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class AdminGuard implements CanActivate {
  constructor(public router: Router) {
  }

  canActivate() {

    try {
      const userType = localStorage.getItem('userType')
      if (userType !== 'admin') {
        this.router.navigateByUrl('/auth/login');
      }
      return true;
    }
    catch (err) {
      return false;
    }
  }

}
