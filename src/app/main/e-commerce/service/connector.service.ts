import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ConnectorService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getConnector(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/connector`, {headers: this.reqHeader});
  }

  getConnectorById(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/connector/${id}`, {headers: this.reqHeader});
  }

  updateConnector(id: any, Connector: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/connector/${id}`, Connector, {headers: this.reqHeader});
  }

  createConnector(Connector: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/connector`, Connector, {headers: this.reqHeader});
  }

  deleteConnector(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/connector/${id}`, {headers: this.reqHeader});
  }
}
