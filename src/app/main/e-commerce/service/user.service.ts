import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { User } from '../model/user.model';

@Injectable()
export class UserService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getUsers(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/user`, {headers: this.reqHeader});
  }

  getUser(id: any): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/user/${id}`, {headers: this.reqHeader});
}

  updateUser(id: any, user: User): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/user/${id}`, user, {headers: this.reqHeader});
  }

  createUser(user: User): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/user`, user, {headers: this.reqHeader});
  }

  generatApiToken(id: any): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/user/${id}/token`, {headers: this.reqHeader});
  }
}
