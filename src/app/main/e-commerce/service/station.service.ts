import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StationService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getStations(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/station`, {headers: this.reqHeader});
  }

  getStation(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/station/${id}`, {headers: this.reqHeader});
  }

  updateStation(id: any, station: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/station/${id}`, station, {headers: this.reqHeader});
  }

  createStation(station: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/station`, station, {headers: this.reqHeader});
  }

  deleteStation(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/station/${id}`, {headers: this.reqHeader});
  }
}
