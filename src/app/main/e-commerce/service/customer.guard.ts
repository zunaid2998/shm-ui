import { Injectable } from '@angular/core';
import {
  CanActivate,
  Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})

export class CustomerGuard implements CanActivate {
  constructor(public router: Router) {
  }

  canActivate() {

    try {
      const userType = localStorage.getItem('userType')
      if (userType !== 'customer') {
        this.router.navigateByUrl('/auth/login');
      }
      return true;
    }
    catch (err) {
      return false;
    }
  }

}
