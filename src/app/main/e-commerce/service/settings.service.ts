import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable()
export class SettingsService {
  apiUrl = environment.api;
  settingsId = environment.settingsId
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json'
  });

  constructor(private _httpClient: HttpClient) {}

  getSettings(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/settings/${this.settingsId}`, {headers: this.reqHeader});
  }

  updateSettings(settings: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/settings/${this.settingsId}`, settings, {headers: this.reqHeader});
  }
}
