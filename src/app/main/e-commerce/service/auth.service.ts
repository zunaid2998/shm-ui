import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  apiUrl = environment.api;

  constructor(private _httpClient: HttpClient) {}

  login(email, password): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/auth/login`, {email, password});
  }

  sendResetPasswordLink(email): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/auth/sendResetPasswordLink`, {email});
  }

  checkVerificationCode(code): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/auth/checkVerificationCode`, {code});
  } 
}
