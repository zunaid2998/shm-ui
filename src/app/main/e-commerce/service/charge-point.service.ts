import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChargePointService {
  apiUrl = environment.api;
  reqHeader = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + localStorage.getItem('token')
  });

  constructor(private _httpClient: HttpClient) {}

  getChargePoint(): Observable<any>{
      return this._httpClient.get(`${this.apiUrl}/chargePoint`, {headers: this.reqHeader});
  }

  getChargePointById(id: string): Observable<any>{
    return this._httpClient.get(`${this.apiUrl}/chargePoint/${id}`, {headers: this.reqHeader});
  }

  updateChargePoint(id: any, chargePoint: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/chargePoint/${id}`, chargePoint, {headers: this.reqHeader});
  }

  createChargePoint(chargePoint: any): Observable<any>{
    return this._httpClient.post(`${this.apiUrl}/chargePoint`, chargePoint, {headers: this.reqHeader});
  }

  deleteChargePoint(id: any): Observable<any>{
    return this._httpClient.delete(`${this.apiUrl}/chargePoint/${id}`, {headers: this.reqHeader});
  }
}
