import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { PaymentTransactionService } from '../service/payment-transaction.service';

@Component({
  selector: 'app-payment-transaction',
  templateUrl: './payment-transaction.component.html',
  styleUrls: ['./payment-transaction.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [fuseAnimations]
})
export class PaymentTransactionComponent implements OnInit {
  paymentTransactionList: any[] = [];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['username', 'appointmentNo', 'amount', 'status', 'stationName', 'dateTime'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private paymentService: PaymentTransactionService,
              private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.paymentTransactionList);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadPaymentTransactionList()
  }

  loadPaymentTransactionList(): void {
    this.loading = true;
    this.paymentService.getPaymentTransaction()
      .subscribe((response: any) => {
        this.loading = false;
        this.paymentTransactionList = response.data;
        this.dataSource.data = this.paymentTransactionList;
      }, error => console.log(error));
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
