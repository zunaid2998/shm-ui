import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MaterialService } from '../../service/material.service';
import { Material } from '../../model/material.model';
import { ToastService } from '../../service/toast.service';

@Component({
  selector: 'app-material-detail',
  templateUrl: './material-detail.component.html',
  styleUrls: ['./material-detail.component.scss']
})
export class MaterialDetailComponent implements OnInit {
  dialogTitle = 'Material Detail';
  material: any;
  mode: any
  constructor(private _matDialogRef: MatDialogRef<MaterialDetailComponent>,
              @Inject(MAT_DIALOG_DATA) private _data: any,
              private materialService: MaterialService,
              private toastService: ToastService) {
    this.material = _data.material || new Material();
    this.mode = _data.mode
  }

  ngOnInit(): void {
   // console.log(this.patient);
  }

  closeDialoag(message?: string): void {
    this._matDialogRef.close(message);
  }

  onClickSave(): void {
    this.materialService.saveMaterial(this.material._id, this.material)
      .subscribe((data: any) => {
        this.closeDialoag('materialSaved');
      }, response => {
        if(response.error.codeName === 'DuplicateKey') this.toastService.show('Duplicate Material No. found. Material No. needs to be unique')
      });
  }

  onClickCreate(): void {
    this.materialService.createMaterial(this.material)
      .subscribe((data: any) => {
        this.closeDialoag('materialSaved');
      }, response => {
        if(response.error.codeName === 'DuplicateKey') this.toastService.show('Duplicate Material No. found. Material No. needs to be unique')
      });
  }

  formValid(): boolean {
    return this.material.number
  }
}
