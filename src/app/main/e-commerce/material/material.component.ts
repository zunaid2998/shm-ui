import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { fuseAnimations } from '@fuse/animations';
import * as _ from 'lodash';
import { MaterialService } from '../service/material.service';
import { Material } from '../model/material.model';
import { MaterialDetailComponent } from './material-detail/material-detail.component';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class MaterialComponent implements OnInit {
  materials: any[];
  loading: boolean;

  dataSource: MatTableDataSource<any>;
  displayedColumns: string[] = ['number', 'description', 'unitOfMeasure', 'status'];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private materialService: MaterialService,
    private _matDialog: MatDialog) {
      this.dataSource = new MatTableDataSource(this.materials);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
      
  }

  ngOnInit(): void {
    this.loadMaterials()
  }

  loadMaterials(): void {
    this.loading = true;
    this.materialService.getMaterials()
      .subscribe((response: any) => {
        this.loading = false;
        this.materials = response.data;
        this.dataSource.data = this.materials;
      }, error => console.log(error));
  }

  onClickMaterialDetail(material: any): void {
    let selectedMaterial: Material = new Material();
    selectedMaterial = _.clone(material);
    const materialDialog = this._matDialog.open(MaterialDetailComponent, {
        panelClass: 'material-detail-dialog',
        width: '600px',
        data: {
          material: selectedMaterial,
          mode: 'update'
        }
    });
    materialDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadMaterials()
      }
    })
  }

  onClickCreateMaterial(): void {
    const materialDialog = this._matDialog.open(MaterialDetailComponent, {
      panelClass: 'material-detail-dialog',
      width: '600px',
      data: {
        material: null,
        mode: 'create'
      }
    });
    materialDialog.afterClosed().subscribe(data => {
      if(data) {
        this.loadMaterials()
      }
    })
  }

  applyFilter(filterValue: string): void {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  getStatus(material: any): string{
    return material.inactive ? 'Inactive' : 'Active'
  }
}
