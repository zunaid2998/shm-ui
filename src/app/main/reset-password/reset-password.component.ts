import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../e-commerce/service/auth.service';
import { ToastService } from '../e-commerce/service/toast.service';
import { UserService } from '../e-commerce/service/user.service';

@Component({
    selector     : 'reset-password',
    templateUrl  : './reset-password.component.html',
    styleUrls    : ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ResetPasswordComponent implements OnInit, OnDestroy
{
    loading: boolean
    verificationCodeInvalid: boolean
    resetPasswordForm: FormGroup;
    user: any
    showResetPasswordSuccessMessage: boolean
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private activatedRoute: ActivatedRoute,
        private authService: AuthService,
        private router: Router,
        private toastService: ToastService,
        private userService: UserService
    )
    {
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void
    {
        this.activatedRoute.params.subscribe(param => {
            if(!param.code) {
                this.router.navigate(['auth', 'login'])
            }
            this.loading = true
            this.authService.checkVerificationCode(param.code)
                .subscribe(response => {
                    if(!response.data) {
                        this.verificationCodeInvalid = true
                    }
                    this.user = response.data
                },(error) => console.log(error), () => this.loading = false)
        })
        this.resetPasswordForm = this._formBuilder.group({
            password       : ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    private checkPasswordStrength = password => {
        let passStrength = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/
        return passStrength.test(password)
    }

    ngOnDestroy(): void
    {
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    onCickResetPassword(): void {
        const password = this.resetPasswordForm.get('password').value
        if(!this.checkPasswordStrength(password)) {
            this.toastService.show('Password must be minimum 8 characters long with min 1 capital letter, min 1 capital letter, 1 special symbol and 1 number', 6000)
            return
        }
        this.user.password = password
        this.userService.updateUser(this.user._id, this.user)
            .subscribe(response => {
                if(response.data) {
                    this.showResetPasswordSuccessMessage = true
                }
            })
    }
}


export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if ( !control.parent || !control )
    {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if ( !password || !passwordConfirm )
    {
        return null;
    }

    if ( passwordConfirm.value === '' )
    {
        return null;
    }

    if ( password.value === passwordConfirm.value )
    {
        return null;
    }

    return {passwordsNotMatching: true};
};
