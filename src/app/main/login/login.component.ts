import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from '../e-commerce/service/auth.service';
import { Router } from '@angular/router';
import { ToastService } from '../e-commerce/service/toast.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { EventService } from '../e-commerce/service/event.service';
import { MatDialog } from '@angular/material';
import { PopupComponent } from './popup/popup.component';
import { environment } from 'environments/environment';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private authService: AuthService,
        private eventService: EventService,
        private router: Router,
        private toastService: ToastService,
        private _fuseNavigationService: FuseNavigationService,
        private _matDialog: MatDialog
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    private logEvent(user: any, eventMessage: string): void {
        this.eventService.createEvent({
            userId: user._id,
            userName: user.firstName + ' ' + user.lastName,
            event: eventMessage
        }).subscribe(() => {})
    }

    private logLoginErrorEvent(email: string, eventMessage: string): void {
        this.eventService.createEvent({
            userId: null,
            userName: email,
            event: eventMessage
        }).subscribe(() => {})
    }

    onSubmit(){
        let emailText = this.loginForm.get('email').value
        const password = this.loginForm.get('password').value
        if(emailText && password){
            const email = emailText.toLowerCase()
            this.authService.login(email, password)
                .subscribe((response: any) => {
                    if(response && response.data) {
                        if(response.data === 'suspended'){
                            this.toastService.show(response.msg)
                            return
                        }
                        const user = response.data
                        this.logEvent(user, 'Login Success')
                        localStorage.setItem('userId', user._id)
                        localStorage.setItem('userType', user.userType)
                        localStorage.setItem('userName', user.firstName + ' ' + user.lastName)
                        localStorage.setItem('email', user.email)
                        this._fuseNavigationService.setupNavigationByUser()
                        if(user.userType === 'admin'){
                            this.router.navigate(['station'])
                        } else {
                            this.router.navigate(['orders'])
                        }
                        if(environment.testing) {
                            this._matDialog.open(PopupComponent, {
                                autoFocus: false,
                                panelClass: 'popup-dialog',
                                width: '600px'
                            })
                        }
                        
                    } else {
                        if(!response.data) {
                            this.logLoginErrorEvent(email, response.msg)
                        }
                        this.toastService.show("Invalid Username / Password Combination")
                    }
                })
        }
    }
}
