import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { MaterialDetailComponent } from 'app/main/e-commerce/material/material-detail/material-detail.component';

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  dialogTitle = 'Warning!';

  constructor(private _matDialogRef: MatDialogRef<MaterialDetailComponent>) {}

  ngOnInit(): void {}

  closeDialoag(): void {
    this._matDialogRef.close();
  }
}
